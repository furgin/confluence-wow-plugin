package com.atlassian.confluence.plugins.wowplugin;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;

/**
 */
public class BandanaPluginConfiguration implements PluginConfiguration
{
    private final BandanaManager bandanaManager;
    private final SpaceManager spaceManager;
    private final PermissionManager permissionManager;

    public BandanaPluginConfiguration(BandanaManager bandanaManager, SpaceManager spaceManager, PermissionManager permissionManager)
    {
        this.bandanaManager = bandanaManager;
        this.spaceManager = spaceManager;
        this.permissionManager = permissionManager;
    }

    public String getAlertSpace()
    {
        return getStringValue("wow.alert.space");
    }

    private String getStringValue(String key)
    {
        return (String) bandanaManager.getValue(new ConfluenceBandanaContext(), key);
    }

    private void setStringValue(String key, String value)
    {
        bandanaManager.setValue(new ConfluenceBandanaContext(), key, value);
    }

    public String getAlertUser()
    {
        return getStringValue("wow.alert.user");
    }

    public String getDefaultRealm()
    {
        return getStringValue("wow.default.realm");
    }

    public String getDefaultRegion()
    {
        return getStringValue("wow.default.region");
    }

    public String getPermissionSpace()
    {
        return getStringValue("wow.permission.space");
    }

    public void setAlertSpace(String alertSpace)
    {
        setStringValue("wow.alert.space", alertSpace);
    }

    public void setAlertUser(String alertUser)
    {
        setStringValue("wow.alert.user", alertUser);
    }

    public void setDefaultRealm(String defaultRealm)
    {
        setStringValue("wow.default.realm", defaultRealm);
    }

    public void setDefaultRegion(String defaultRegion)
    {
        setStringValue("wow.default.region", defaultRegion);
    }

    public void setPermissionSpace(String permissionSpace)
    {
        setStringValue("wow.permission.space", permissionSpace);
    }

    public boolean isPermittedToEditRaids()
    {
        return isPermittedToCreateRaids();
    }

    public boolean isPermittedToCreateRaids()
    {
        Space space = spaceManager.getSpace(getPermissionSpace());
        if (space == null)
            return false;
        final User user = AuthenticatedUserThreadLocal.get();
        return !(user == null || !permissionManager.hasCreatePermission(user, space, Page.class));
    }
}
