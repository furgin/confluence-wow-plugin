package com.atlassian.confluence.plugins.wowplugin.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "raid")
@XmlAccessorType(XmlAccessType.FIELD)
public class RaidEntity
{
    @XmlAttribute
    private String key;

    @XmlAttribute
    private boolean invite;

    public RaidEntity()
    {
    }

    public RaidEntity(String key, boolean invite)
    {
        this.key = key;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public boolean isInvite()
    {
        return invite;
    }

    public void setInvite(boolean invite)
    {
        this.invite = invite;
    }
}
