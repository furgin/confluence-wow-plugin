package com.atlassian.confluence.plugins.wowplugin.rest;

import au.com.angry.battlenet.model.CharacterRole;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.RaidPlayer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "rsvp")
@XmlAccessorType(XmlAccessType.FIELD)
public class RsvpEntity
{
    @XmlAttribute
    private String character;

    @XmlAttribute
    private CharacterRole role;

    @XmlAttribute
    private RaidPlayer.Status status;

    public RsvpEntity()
    {
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public CharacterRole getRole() {
        return role;
    }

    public void setRole(CharacterRole role) {
        this.role = role;
    }

    public RaidPlayer.Status getStatus() {
        return status;
    }

    public void setStatus(RaidPlayer.Status status) {
        this.status = status;
    }
}
