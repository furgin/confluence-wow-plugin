package com.atlassian.confluence.plugins.wowplugin;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.Character;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

public class MacroHelper
{
    private final XhtmlContent xhtmlContent;

    public MacroHelper(XhtmlContent xhtmlContent)
    {
        this.xhtmlContent = xhtmlContent;
    }

    public String characterMacro(BattleNET.Region region, Character ch, ConversionContext conversionContext)
    {
        try
        {
            return xhtmlContent.convertStorageToView(
                    String.format("<ac:macro ac:name=\"wow-character\">" +
                            "<ac:parameter ac:name=\"region\">%s</ac:parameter>" +
                            "<ac:parameter ac:name=\"name\">%s</ac:parameter>" +
                            "<ac:parameter ac:name=\"realm\">%s</ac:parameter>" +
                            "</ac:macro>", region.toString().toLowerCase(), ch.getName(), ch.getRealm()), conversionContext);
        }
        catch (Exception e)
        {
            return "Error: " + e.getMessage();
        }
    }

    public String itemMacro(BattleNET.Region region, long itemId, long[] bonusList, ConversionContext conversionContext)
    {
        try
        {
            String bonusListString = LongList.asString(bonusList);
            return xhtmlContent.convertStorageToView(
                    String.format("<ac:macro ac:name=\"wow-item\">" +
                            "<ac:parameter ac:name=\"region\">%s</ac:parameter>" +
                            "<ac:parameter ac:name=\"witem\">%d</ac:parameter>" +
                            "<ac:parameter ac:name=\"bonusList\">%s</ac:parameter>" +
                            "</ac:macro>", region.toString().toLowerCase(), itemId, bonusListString),
                    conversionContext);
        }
        catch (Exception e)
        {
            return "Error: " + e.getMessage();
        }
    }

    // Not a macro yet
    public String achievementMacro(BattleNET.Region region, long achievementId, ConversionContext conversionContext)
    {
        try
        {
            return xhtmlContent.convertStorageToView(
                    String.format("<ac:macro ac:name=\"wow-achievement\">" +
                            "<ac:parameter ac:name=\"region\">%s</ac:parameter>" +
                            "<ac:parameter ac:name=\"aitem\">%d</ac:parameter>" +
                            "</ac:macro>", region.toString().toLowerCase(), achievementId),
                    conversionContext);
        }
        catch (Exception e)
        {
            return "Error: " + e.getMessage();
        }

    }
}
