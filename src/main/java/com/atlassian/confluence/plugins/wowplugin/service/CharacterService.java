package com.atlassian.confluence.plugins.wowplugin.service;

public interface CharacterService
{
    void removeCharacter(String owner, String region, String realm, String name);
}
