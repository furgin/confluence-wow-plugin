package com.atlassian.confluence.plugins.wowplugin;

import com.atlassian.confluence.plugins.wowplugin.service.BattleNetService;

/**
 */
public class CharacterMacro extends AbstractCharacterMacro
{

    protected CharacterMacro(BattleNetService battleNetService, PluginConfiguration pluginConfiguration)
    {
        super(battleNetService, pluginConfiguration);
    }

    protected String getTemplate()
    {
        return "com/atlassian/confluence/plugins/wowplugin/character.vm";
    }
}