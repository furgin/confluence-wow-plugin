package com.atlassian.confluence.plugins.wowplugin;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.Character;
import au.com.angry.battlenet.model.Guild;
import au.com.angry.battlenet.model.GuildNewsItem;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.wowplugin.service.BattleNetService;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang.StringUtils;
//import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 */
public class GuildActivityMacro implements Macro {
    private final BattleNetService battleNetService;
    private final PluginConfiguration pluginConfiguration;
    private final MacroHelper macroHelper;

    public GuildActivityMacro(BattleNetService battleNetService, XhtmlContent xhtmlContent, PluginConfiguration pluginConfiguration) {
        this.battleNetService = battleNetService;
        this.pluginConfiguration = pluginConfiguration;
        this.macroHelper = new MacroHelper(xhtmlContent);
    }

    public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException {
        String guildName = params.get("guild");
        String realmName = params.get("realm");
        String regionName = params.get("region");

        if (StringUtils.isEmpty(realmName))
            realmName = pluginConfiguration.getDefaultRealm();

        if (StringUtils.isEmpty(regionName))
            regionName = pluginConfiguration.getDefaultRegion();

        BattleNET.Region region = BattleNET.Region.valueOf(regionName.toUpperCase());

        int length = 10;

        if (params.containsKey("length")) {
            try {
                length = Integer.parseInt(params.get("length"));
            } catch (NumberFormatException e) {
                // ignore - use default
            }
        }

        final Guild guild = battleNetService.guild(region, realmName, guildName, Guild.Field.NEWS);

        if (guild != null) {
            StringBuilder out = new StringBuilder("<table>");
            final GuildNewsItem[] news = guild.getNews();
            int index = 0;
            for (GuildNewsItem item : news) {
                Character ch;

                if (item == null || item.getType() == null)
                    continue;

                switch (item.getType()) {
                    case ITEM_LOOT:
                    case ITEM_CRAFT:
                    case ITEM_PURCHASE:
                        ch = battleNetService.character(region, realmName, item.getCharacter());
                        out.append(String.format("<tr><td>%s&nbsp;%s %s.</td></tr>",
                                macroHelper.characterMacro(region, ch, conversionContext),
                                describe(item.getType()),
                                macroHelper.itemMacro(region, item.getItemId(), item.getBonusLists(), conversionContext)));
                        break;
                    case PLAYER_ACHIEVEMENT:
                        ch = battleNetService.character(region, realmName, item.getCharacter());
                        out.append(String.format("<tr><td>%s&nbsp;earned %s.</td></tr>",
                                macroHelper.characterMacro(region, ch, conversionContext),
                                macroHelper.achievementMacro(
                                        region,
                                        item.getAchievement().getId(),
                                        conversionContext)));
                        break;
                }
                index++;
                if (index >= length)
                    break;
            }

            out.append("</table>");



            return out.toString();
        } else {
            return "Could not load guild: " + guildName;
        }

    }

    private String describe(GuildNewsItem.Type type) {
        // replace with i18n
        switch (type) {
            case ITEM_CRAFT:
                return "crafted";
            case ITEM_LOOT:
                return "looted";
            case ITEM_PURCHASE:
                return "purchased";
            default:
                return type.toString();
        }
    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
