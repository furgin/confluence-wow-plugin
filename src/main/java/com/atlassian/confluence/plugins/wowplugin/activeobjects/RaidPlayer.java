package com.atlassian.confluence.plugins.wowplugin.activeobjects;

import au.com.angry.battlenet.model.CharacterRole;
import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

@Table("RAID_PLAYER")
public interface RaidPlayer extends Entity
{
    enum Status { INVITED, CONFIRMED, MAYBE, OUT }

    @NotNull
    Raid getRaid();

    @Accessor("USERNAME")
    String getUsername();

    @Accessor("EMAIL")
    String getEmail();

    @Accessor("CHARACTER")
    String getCharacter();

    @Accessor("STATUS")
    Status getStatus();

    @Accessor("ROLE")
    CharacterRole getRole();

    void setStatus(Status status);

    void setCharacter(String character);

    void setRole(CharacterRole role);
}
