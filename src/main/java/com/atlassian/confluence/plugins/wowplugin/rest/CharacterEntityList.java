package com.atlassian.confluence.plugins.wowplugin.rest;

import com.atlassian.confluence.plugins.wowplugin.activeobjects.CharacterMapping;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 */
@XmlRootElement(name="characters")
@XmlAccessorType(XmlAccessType.FIELD)
public class CharacterEntityList
{
    @XmlElement(name = "character")
    private List<CharacterEntity> characters;

    public CharacterEntityList()
    {
    }

    public CharacterEntityList(List<CharacterMapping> mappings)
    {
        this.characters = new ArrayList<CharacterEntity>();
        for (CharacterMapping character : mappings)
            this.characters.add(new CharacterEntity(character));
    }

    public List<CharacterEntity> getCharacters()
    {
        return characters;
    }

    public void setCharacters(List<CharacterEntity> characters)
    {
        this.characters = characters;
    }
}
