package com.atlassian.confluence.plugins.wowplugin;

/**
 */
public interface PluginConfiguration
{
    String getPermissionSpace();

    String getAlertSpace();

    String getAlertUser();

    String getDefaultRealm();

    String getDefaultRegion();

    void setPermissionSpace(String permissionSpace);

    void setAlertSpace(String alertSpace);

    void setAlertUser(String alertUser);

    void setDefaultRealm(String defaultRealm);

    void setDefaultRegion(String defaultRegion);

    boolean isPermittedToCreateRaids();

    boolean isPermittedToEditRaids();

}
