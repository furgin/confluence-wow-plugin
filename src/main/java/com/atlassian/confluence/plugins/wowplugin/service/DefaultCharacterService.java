package com.atlassian.confluence.plugins.wowplugin.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.CharacterMapping;
import com.atlassian.sal.api.transaction.TransactionCallback;

public class DefaultCharacterService implements CharacterService {
    private final ActiveObjects activeObjects;

    public DefaultCharacterService(ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }

    @Override
    public void removeCharacter(final String owner, final String region, final String realm, final String name) {
        activeObjects.executeInTransaction((TransactionCallback<Void>) () -> {
            CharacterMapping[] mappings = activeObjects.find(CharacterMapping.class,
                    "REALM = ? AND NAME = ? AND OWNER = ? AND REGION = ?", realm, name, owner, region);
            for (CharacterMapping mapping : mappings)
                activeObjects.delete(mapping);
            return null;
        });
    }
}
