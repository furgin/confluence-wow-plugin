package com.atlassian.confluence.plugins.wowplugin.alert;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.wowplugin.PluginConfiguration;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import com.atlassian.renderer.wysiwyg.WysiwygConverter;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.quartz.JobDetail;

/**
 */
public class AlertJobDetail extends JobDetail
{
    private final HttpRetrievalService httpRetrievalService;
    private final SearchManager searchManager;
    private final PageManager pageManager;
    private final TransactionTemplate transactionTemplate;
    private final SpaceManager spaceManager;
    private final WysiwygConverter wysiwygConverter;
    private final PluginConfiguration pluginConfiguration;

    public AlertJobDetail(HttpRetrievalService httpRetrievalService, SearchManager searchManager,
                          PageManager pageManager, SpaceManager spaceManager, TransactionTemplate transactionTemplate,
                          WysiwygConverter wysiwygConverter, PluginConfiguration pluginConfiguration)
    {
        this.searchManager = searchManager;
        this.pageManager = pageManager;
        this.transactionTemplate = transactionTemplate;
        this.spaceManager = spaceManager;
        this.wysiwygConverter = wysiwygConverter;
        this.pluginConfiguration = pluginConfiguration;
        setName(AlertJobDetail.class.getName());
        setJobClass(AlertJob.class);
        this.httpRetrievalService = httpRetrievalService;
    }

    public HttpRetrievalService getHttpRetrievalService()
    {
        return httpRetrievalService;
    }

    public SearchManager getSearchManager()
    {
        return searchManager;
    }

    public PageManager getPageManager()
    {
        return pageManager;
    }

    public SpaceManager getSpaceManager()
    {
        return spaceManager;
    }

    public TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    public WysiwygConverter getWysiwygConverter()
    {
        return wysiwygConverter;
    }

    public PluginConfiguration getPluginConfiguration()
    {
        return pluginConfiguration;
    }
}
