package com.atlassian.confluence.plugins.wowplugin.service;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.CharacterClass;
import au.com.angry.battlenet.model.CharacterRace;

public interface BattleNetService extends BattleNET
{
    CharacterRace characterRace(Region region, int raceId);
    CharacterClass characterClass(Region region, int classId);
}
