package com.atlassian.confluence.plugins.wowplugin;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.Character;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 */
public abstract class AbstractCharacterMacro implements Macro
{
    private final PluginConfiguration pluginConfiguration;
    private final BattleNET battleNET;

    protected AbstractCharacterMacro(BattleNET battleNET, PluginConfiguration pluginConfiguration)
    {
        this.battleNET = battleNET;
        this.pluginConfiguration = pluginConfiguration;
    }

    protected abstract String getTemplate();

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.INLINE;
    }


    public String execute(Map<String, String> params, String body, ConversionContext context) throws MacroExecutionException
    {
        String realm = params.get("realm");
        String name = params.get("name");
        String regionName = params.get("region");

        if (StringUtils.isEmpty(realm))
            realm = pluginConfiguration.getDefaultRealm();

        if (StringUtils.isEmpty(regionName))
            regionName = pluginConfiguration.getDefaultRegion();

        final BattleNET.Region region = BattleNET.Region.valueOf(regionName.toUpperCase());
        Character character = battleNET.character(region, realm, name);
        Map<String,Object> contextMap = new HashMap<>(MacroUtils.defaultVelocityContext());
        contextMap.put("character", character);
        contextMap.put("region", region.toString().toLowerCase());
        return VelocityUtils.getRenderedTemplate(getTemplate(), contextMap);
    }

}