package com.atlassian.confluence.plugins.wowplugin;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.Item;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.wowplugin.item.ItemMapper;
import com.atlassian.confluence.plugins.wowplugin.service.BattleNetService;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class ItemMacro implements Macro {
    private static final Logger log = LoggerFactory.getLogger(ItemMacro.class);

    private final PluginConfiguration pluginConfiguration;
    private final ItemMapper itemMapper;
    private final BattleNetService battleNET;

    public ItemMacro(PluginConfiguration pluginConfiguration, ItemMapper itemMapper, BattleNetService battleNET) {
        this.pluginConfiguration = pluginConfiguration;
        this.itemMapper = itemMapper;
        this.battleNET = battleNET;
    }

    public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException {
        String witemString = params.containsKey("witem") ? params.get("witem") : params.get("0");
        String bonusListString = params.containsKey("bonusList") ? params.get("bonusList") : "";
        String regionName = params.get("region");

        if (StringUtils.isEmpty(regionName))
            regionName = pluginConfiguration.getDefaultRegion();

        BattleNET.Region region = BattleNET.Region.valueOf(regionName.toUpperCase());

        // TODO: validate the bonus list
        long[] bonusList = null;
        try {
            bonusList = LongList.parse(bonusListString);
        } catch (NumberFormatException e) {
            // ignore
        }

        Item item;
        try {
            item = battleNET.item(region, Long.parseLong(witemString), bonusList);
        } catch (NumberFormatException e) {
            long id = itemMapper.getItemId(witemString);
            item = battleNET.item(region, id, bonusList);
        }
        Map<String, Object> contextMap = new HashMap<String, Object>(MacroUtils.defaultVelocityContext());
        contextMap.put("item", item);
        if(bonusList!=null && bonusList.length>=1) {
            contextMap.put("bonus", bonusList[0]);
            contextMap.put("bonusList", bonusListString);
        }
        return VelocityUtils.getRenderedTemplate("/com/atlassian/confluence/plugins/wowplugin/wow-item.vm", contextMap);

    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}