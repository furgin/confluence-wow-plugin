package com.atlassian.confluence.plugins.wowplugin.item;

/**
 */
public interface ItemMapper
{
    long getItemId(String name);
}
