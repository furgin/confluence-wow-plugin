package com.atlassian.confluence.plugins.wowplugin.activeobjects;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.NotNull;

/**
 * Stores the link between a World of Warcraft character and a Confluence user.
 */
public interface CharacterMapping extends Entity
{
    @Indexed
    @NotNull
    @Accessor("NAME")
    String getName();

    @Indexed
    @NotNull
    @Accessor("REALM")
    String getRealm();

    @Indexed
    @NotNull
    @Accessor("REGION")
    String getRegion();

    @Indexed
    @NotNull
    @Accessor("OWNER")
    String getOwner();

}
