package com.atlassian.confluence.plugins.wowplugin.alert;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.wowplugin.PluginConfiguration;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.*;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.CreatorQuery;
import com.atlassian.confluence.search.v2.sort.CreatedSort;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.ConfluenceUserManager;
import com.atlassian.confluence.util.http.HttpResponse;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import com.atlassian.quartz.jobs.AbstractJob;
import com.atlassian.renderer.wysiwyg.WysiwygConverter;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.user.EntityException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 */
public class AlertJob extends AbstractJob {
    private static final DateFormat format = new SimpleDateFormat("yyyyMMddss");
    private static final Logger log = LoggerFactory.getLogger(AlertJob.class);

    private ConfluenceUserManager confluenceUserManager;

    public AlertJob() {
    }

    public void setConfluenceUserManager(ConfluenceUserManager confluenceUserManager) {
        this.confluenceUserManager = confluenceUserManager;
    }

    public void doExecute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        AlertJobDetail jobDetail = (AlertJobDetail) jobExecutionContext.getJobDetail();
        HttpRetrievalService httpRetrievalService = jobDetail.getHttpRetrievalService();
        SearchManager searchManager = jobDetail.getSearchManager();
        final PageManager pageManager = jobDetail.getPageManager();
        final SpaceManager spaceManager = jobDetail.getSpaceManager();
        final TransactionTemplate transactionTemplate = jobDetail.getTransactionTemplate();
        final WysiwygConverter wysiwygConverter = jobDetail.getWysiwygConverter();
        final PluginConfiguration pluginConfiguration = jobDetail.getPluginConfiguration();

        if (StringUtils.isEmpty(pluginConfiguration.getAlertUser()) || StringUtils.isEmpty(pluginConfiguration.getAlertSpace()))
            return;

        try {
            HttpResponse httpResponse = httpRetrievalService.get("http://launcher.worldofwarcraft.com/alert");
            final String content = wysiwygConverter.convertXHtmlToWikiMarkup(IOUtils.toString(httpResponse.getResponse()));

            Collection<SearchQuery> must = new ArrayList<>();
            must.add(new CreatorQuery("alert"));
            must.add(new ContentTypeQuery(ContentTypeEnum.BLOG));
            SearchQuery query = new BooleanQuery(must, null, null);
            ISearch search = new ContentSearch(query,
                    new CreatedSort(SearchSort.Order.DESCENDING),
                    null, null);
            List<Searchable> searchables = searchManager.searchEntities(search, SearchManager.EntityVersionPolicy.LATEST_VERSION);
            log.debug("size: " + searchables.size());

            boolean createPost = true;
            if (content.trim().length() == 0) {
                log.debug("no alert - not creating blog post");
                createPost = false;
            }

            if (searchables.size() > 0 && createPost) {
                BlogPost post = (BlogPost) searchables.get(0);
                if (post.getBodyAsString().equals(content)) {
                    log.debug("no changes - not creating blog post");
                    createPost = false;
                }
            }

            if (createPost) {
                transactionTemplate.execute(() -> {
                    BlogPost post = new BlogPost();
                    post.setTitle("Server Alert - " + format.format(new Date()));
                    try {
                        post.setCreator(new ConfluenceUserImpl(confluenceUserManager.getUser(pluginConfiguration.getAlertUser())));
                    } catch (EntityException e) {
                        log.error("cannot find creator user [ " + e.getMessage() + " ]");
                    }
                    post.setSpace(spaceManager.getSpace(pluginConfiguration.getAlertSpace()));
                    post.setBodyAsString(content);
                    post.setCreationDate(new Date());
                    pageManager.saveContentEntity(post, new DefaultSaveContext());
                    return post;
                });
            }

        } catch (InvalidSearchException e) {
            log.error("invalid search: ", e);
        } catch (IOException e) {
            log.error("could not retrieve launcher alert: ", e);
        }

    }
}
