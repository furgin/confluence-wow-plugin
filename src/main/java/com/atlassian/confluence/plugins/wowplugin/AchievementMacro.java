package com.atlassian.confluence.plugins.wowplugin;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.Achievement;
import au.com.angry.battlenet.model.AchievementCategory;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.wowplugin.service.BattleNetService;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import org.apache.commons.lang.StringUtils;
import com.atlassian.plugin.util.collect.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class AchievementMacro implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(AchievementMacro.class);

    private final BattleNetService battleNetService;
    private final PluginConfiguration pluginConfiguration;

    public AchievementMacro(BattleNetService battleNetService, PluginConfiguration pluginConfiguration)
    {
        this.battleNetService = battleNetService;
        this.pluginConfiguration = pluginConfiguration;
    }

    public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        final String aitemString = params.containsKey("aitem") ? params.get("aitem") : params.get("0");
        String regionName = params.get("region");
        if (StringUtils.isEmpty(regionName))
            regionName = pluginConfiguration.getDefaultRegion();
        BattleNET.Region region = BattleNET.Region.valueOf(regionName.toUpperCase());

        final AchievementCategory[] achievements = battleNetService.achievements(region);
        Achievement achievement;
        try
        {
            final int id = Integer.parseInt(aitemString);
            achievement = find(achievements, achievement1 -> achievement1.getId() == id);
        }
        catch (NumberFormatException e)
        {
            achievement = find(achievements, achievement12 -> achievement12.getTitle().equals(aitemString));
        }

        Map<String,Object> contextMap = new HashMap<>(MacroUtils.defaultVelocityContext());
        contextMap.put("achievement", achievement);
        return VelocityUtils.getRenderedTemplate("/com/atlassian/confluence/plugins/wowplugin/achievement.vm", contextMap);

    }

    private Achievement find(AchievementCategory[] categories, Predicate<Achievement> predicate)
    {
        if(categories==null)
            return null;

        for (AchievementCategory category : categories)
        {
            for (Achievement a : category.getAchievements())
                if (predicate.evaluate(a))
                    return a;
            Achievement a = find(category.getCategories(), predicate);
            if (a != null)
                return a;
        }
        return null;
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.INLINE;
    }
}