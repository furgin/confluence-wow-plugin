package com.atlassian.confluence.plugins.wowplugin.raid;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.Raid;
import com.atlassian.confluence.plugins.wowplugin.service.RaidService;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListRaidsMacro implements Macro
{
    private final RaidService raidService;

    public ListRaidsMacro(RaidService raidService)
    {
        this.raidService = raidService;
    }

    public String execute(Map<String, String> params, String body, ConversionContext context) throws MacroExecutionException
    {
        String countString = params.get("count");
        int count = 5;
        try
        {
            count = Integer.parseInt(countString);
        }
        catch (NumberFormatException e)
        {
            // ignore - use default
        }

        Map<String,Object> contextMap = new HashMap<String, Object>(MacroUtils.defaultVelocityContext());
        final List<Raid> allRaids = raidService.getRaids();
        contextMap.put("raids", allRaids.subList(0, Math.min(allRaids.size(), count)));
        return VelocityUtils.getRenderedTemplate("com/atlassian/confluence/plugins/wowplugin/raid/list.vm", contextMap);
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
}
