package com.atlassian.confluence.plugins.wowplugin.theme;

import com.atlassian.confluence.themes.BasicTheme;

public class ANBTheme extends BasicTheme {

    @Override
    public boolean hasSpaceSideBar() {
        return false;
    }
}
