package com.atlassian.confluence.plugins.wowplugin.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugins.wowplugin.PluginConfiguration;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.sal.api.message.I18nResolver;
import com.opensymphony.xwork.ActionSupport;

/**
 */
public class ConfigurationAction extends ConfluenceActionSupport
{
    private String alertSpace;
    private String alertUser;
    private String defaultRealm;
    private String defaultRegion;
    private String permissionSpace;
    private PermissionManager permissionManager;
    private PluginConfiguration pluginConfiguration;
    private boolean editMode = false;

    public ConfigurationAction()
    {
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setPluginConfiguration(PluginConfiguration pluginConfiguration)
    {
        this.pluginConfiguration = pluginConfiguration;
    }

    public boolean isPermitted()
    {
        return permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(),
                Permission.ADMINISTER,
                PermissionManager.TARGET_APPLICATION);
    }

    public String save() throws Exception
    {
        pluginConfiguration.setAlertSpace(alertSpace);
        pluginConfiguration.setAlertUser(alertUser);
        pluginConfiguration.setDefaultRealm(defaultRealm);
        pluginConfiguration.setDefaultRegion(defaultRegion);
        pluginConfiguration.setPermissionSpace(permissionSpace);
        return SUCCESS;
    }

    public String edit() throws Exception
    {
        alertSpace = pluginConfiguration.getAlertSpace();
        alertUser = pluginConfiguration.getAlertUser();
        defaultRealm = pluginConfiguration.getDefaultRealm();
        defaultRegion = pluginConfiguration.getDefaultRegion();
        permissionSpace = pluginConfiguration.getPermissionSpace();
        editMode = true;
        return SUCCESS;
    }

    public String view() throws Exception
    {
        alertSpace = pluginConfiguration.getAlertSpace();
        alertUser = pluginConfiguration.getAlertUser();
        defaultRealm = pluginConfiguration.getDefaultRealm();
        defaultRegion = pluginConfiguration.getDefaultRegion();
        permissionSpace = pluginConfiguration.getPermissionSpace();
        editMode = false;
        return SUCCESS;
    }

    public String getAlertSpace()
    {
        return alertSpace;
    }

    public void setAlertSpace(String alertSpace)
    {
        this.alertSpace = alertSpace;
    }

    public String getAlertUser()
    {
        return alertUser;
    }

    public void setAlertUser(String alertUser)
    {
        this.alertUser = alertUser;
    }

    public String getDefaultRealm()
    {
        return defaultRealm;
    }

    public void setDefaultRealm(String defaultRealm)
    {
        this.defaultRealm = defaultRealm;
    }

    public String getDefaultRegion()
    {
        return defaultRegion;
    }

    public void setDefaultRegion(String defaultRegion)
    {
        this.defaultRegion = defaultRegion;
    }

    public String getPermissionSpace()
    {
        return permissionSpace;
    }

    public void setPermissionSpace(String permissionSpace)
    {
        this.permissionSpace = permissionSpace;
    }

    public boolean isEditMode()
    {
        return editMode;
    }

    public void setEditMode(boolean editMode)
    {
        this.editMode = editMode;
    }

}
