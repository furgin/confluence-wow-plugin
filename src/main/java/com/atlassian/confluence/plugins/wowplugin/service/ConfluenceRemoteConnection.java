package com.atlassian.confluence.plugins.wowplugin.service;

import au.com.angry.battlenet.BattleNETException;
import au.com.angry.remote.Param;
import au.com.angry.remote.RemoteConnection;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.URI;

public class ConfluenceRemoteConnection implements RemoteConnection
{
    private final Logger logger = LoggerFactory.getLogger(ConfluenceRemoteConnection.class);
    private final HttpRetrievalService httpRetrievalService;

    public ConfluenceRemoteConnection(HttpRetrievalService httpRetrievalService)
    {
        this.httpRetrievalService = httpRetrievalService;
    }

    public InputStream load(String host, String path, Param... params)
    {
        try
        {
            URI uri = new URI("https", host, path, q(params), null);
            logger.debug("load [ " + uri.toASCIIString() + " ]");
            return httpRetrievalService.get(uri.toASCIIString()).getResponse();
        }
        catch (Exception e)
        {
            throw new BattleNETException(path, e);
        }
    }

    private String q(Param... params)
    {
        if (params == null || params.length == 0) return null;
        StringBuilder b = new StringBuilder();
        for (Param param : params)
        {
            if (b.length() > 0)
                b.append("&");
            b.append(param.getName()).append("=").append(param.getValue());
        }
        return b.toString();
    }
}
