package com.atlassian.confluence.plugins.wowplugin.item;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.util.http.HttpResponse;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 */
public class ItemMapperImpl implements ItemMapper {
    private static final Logger log = LoggerFactory.getLogger(ItemMapperImpl.class);

    private final HttpRetrievalService httpRetrievalService;
    private final CacheManager cacheManager;

    public ItemMapperImpl(HttpRetrievalService httpRetrievalService, CacheManager cacheManager) {
        this.httpRetrievalService = httpRetrievalService;
        this.cacheManager = cacheManager;
    }

    @Nonnull
    private Cache<String, Long> getCache() {
        return cacheManager.getCache(ItemMapper.class.getName());
    }

    @Override
    public long getItemId(String name) {

        if (getCache().containsKey(name)) {
            Long itemId = getCache().get(name);
            if (itemId != null) {
                log.debug(String.format("found cached item id [ %d ] from name [ " + name + " ]", itemId, name));
                return itemId;
            }
        }

        try {
            String encodedRealName = enc(name);
            String wowpediaUrl = "http://www.wowpedia.org/" + encodedRealName;
            log.debug("asking wowpedia for item id for [ " + name + " ] from [ " + wowpediaUrl + " ]");
            HttpResponse wowpediaItem = httpRetrievalService.get(wowpediaUrl);
            Document doc = Jsoup.parse(IOUtils.toString(wowpediaItem.getResponse()));
            // eg. http://www.wowhead.com/item=65049
            String wowheadHref = doc.select("li.wowhead a").attr("href");
            long itemId = Long.parseLong(wowheadHref.substring(wowheadHref.lastIndexOf("=") + 1));
            log.debug(String.format("found item id [ %d ] from name [ " + name + " ]", itemId, name));
            getCache().put(name, itemId);
            return itemId;
        } catch (IOException e) {
            throw new RuntimeException("could not look up item by name [ " + name + " ]", e);
        }
    }

    private String enc(String name) {
        return name == null ? null : name.replace(" ", "_");
    }
}
