package com.atlassian.confluence.plugins.wowplugin.actions;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugins.wowplugin.PluginConfiguration;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.Raid;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.RaidPlayer;

/**
 * Action to list linked World of Warcraft Characters
 */
public class ViewRaidAction extends ConfluenceActionSupport
{
    private String key;
    private final ActiveObjects activeObjects;
    private final PluginConfiguration pluginConfiguration;
    private Raid raid;

    public ViewRaidAction(ActiveObjects activeObjects, PluginConfiguration pluginConfiguration)
    {
        this.activeObjects = activeObjects;
        this.pluginConfiguration = pluginConfiguration;
    }

    public String execute() throws Exception
    {
        raid = activeObjects.executeInTransaction(() -> {
            final Raid[] raids = activeObjects.find(Raid.class, "KEY = ?", key);
            return raids.length==0 ? null : raids[0];
        });
        return SUCCESS;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public Raid getRaid()
    {
        return raid;
    }

    public RaidPlayer.Status[] getStatuses()
    {
        return RaidPlayer.Status.values();
    }

    public boolean isPermittedToEditRaids()
    {
        return pluginConfiguration.isPermittedToEditRaids();
    }
}
