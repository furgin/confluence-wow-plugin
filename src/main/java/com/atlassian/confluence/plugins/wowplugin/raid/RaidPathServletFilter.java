package com.atlassian.confluence.plugins.wowplugin.raid;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RaidPathServletFilter implements Filter
{
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        String path = req.getRequestURI();

        if (!path.endsWith(".action"))
        {
            int index = 0;
            String[] split = StringUtils.split(path, "/");

            if(("/"+split[index]).equals(req.getContextPath()))
                index++;

            if(split[index].equals("raid"))
                index++;

            if(index>split.length-1) {
                req.getRequestDispatcher("/raid/list.action").forward(req, resp);
            } else {
                req.getRequestDispatcher("/raid/view.action?key=" + split[index]).forward(req, resp);
            }
        }
        else
        {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    public void destroy()
    {
    }

}
