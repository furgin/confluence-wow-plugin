package com.atlassian.confluence.plugins.wowplugin.raid;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.wowplugin.PluginConfiguration;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CreateRaidMacro implements Macro
{
    private final PluginConfiguration pluginConfiguration;

    public CreateRaidMacro(PluginConfiguration pluginConfiguration)
    {
        this.pluginConfiguration = pluginConfiguration;
    }


    public String execute(Map<String, String> params, String body, ConversionContext context) throws MacroExecutionException
    {
        Map<String,Object> contextMap = new HashMap<String, Object>(MacroUtils.defaultVelocityContext());
        contextMap.put("permitted", pluginConfiguration.isPermittedToCreateRaids());
        contextMap.put("uuid", UUID.randomUUID().toString());
        return VelocityUtils.getRenderedTemplate("com/atlassian/confluence/plugins/wowplugin/raid/create.vm", contextMap);
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.INLINE;
    }
}
