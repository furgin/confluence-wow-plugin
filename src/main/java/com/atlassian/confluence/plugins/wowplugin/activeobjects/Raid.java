package com.atlassian.confluence.plugins.wowplugin.activeobjects;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

import java.util.Date;

@Table("RAID")
public interface Raid extends Entity
{
    @NotNull
    @Accessor("CREATED_DATE")
    Date getCreatedDate();

    @Indexed
    @NotNull
    @Accessor("KEY")
    String getKey();

    @Accessor("DATE")
    Date getDate();

    @Accessor("ZONE")
    String getZone();

    @Accessor("REALM")
    String getRealm();

    @Accessor("REGION")
    String getRegion();

    @OneToMany
    RaidPlayer[] getPlayers();
}
