package com.atlassian.confluence.plugins.wowplugin.service;

import com.atlassian.confluence.plugins.wowplugin.activeobjects.Raid;
import com.atlassian.confluence.plugins.wowplugin.rest.RsvpEntity;

import java.util.List;

public interface RaidService
{
    void deleteRaid(String key);

    void createRaid(String key, boolean invite);

    void deleteFromRaid(String key, String email);

    void inviteToRaid(String key, String email);

    void rsvpToRaid(String key, String email, RsvpEntity status);

    List<Raid> getRaids();

}
