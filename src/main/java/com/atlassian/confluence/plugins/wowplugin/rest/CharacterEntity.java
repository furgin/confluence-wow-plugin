package com.atlassian.confluence.plugins.wowplugin.rest;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.Character;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.CharacterMapping;
import com.atlassian.confluence.plugins.wowplugin.service.BattleNetService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 */
@XmlRootElement(name = "character")
@XmlAccessorType(XmlAccessType.FIELD)
public class CharacterEntity
{
    @XmlAttribute
    private String name;

    @XmlAttribute
    private String realm;

    @XmlAttribute
    private String race;

    @XmlAttribute
    private String region;

    @XmlAttribute
    private int level;

    @XmlAttribute
    private int averageItemLevel;

    @XmlAttribute(name = "class")
    private String characterClass;

    @XmlAttribute(name = "thumbnail")
    private String thumbnail;

    public CharacterEntity()
    {
    }

    public CharacterEntity(String name, String realm, String region)
    {
        this.name = name;
        this.realm = realm;
        this.region = region;
    }

    public CharacterEntity(CharacterMapping character)
    {
        this.name = character.getName();
        this.realm = character.getRealm();
        this.region = character.getRegion();
    }

    public CharacterEntity(BattleNET.Region region, Character character, BattleNetService battleNetService)
    {
        this.name = character.getName();
        this.realm = character.getRealm();
        this.race = battleNetService.characterRace(region, character.getRace()).getName();
        this.characterClass = battleNetService.characterClass(region, character.getCharacterClass()).getName();
        this.averageItemLevel = character.getItems().getAverageItemLevelEquipped();
        this.level = character.getLevel();
        this.thumbnail = character.getThumbnail();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getRealm()
    {
        return realm;
    }

    public void setRealm(String realm)
    {
        this.realm = realm;
    }

    public String getRace()
    {
        return race;
    }

    public void setRace(String race)
    {
        this.race = race;
    }

    public String getCharacterClass()
    {
        return characterClass;
    }

    public void setCharacterClass(String characterClass)
    {
        this.characterClass = characterClass;
    }

    public int getAverageItemLevel()
    {
        return averageItemLevel;
    }

    public void setAverageItemLevel(int averageItemLevel)
    {
        this.averageItemLevel = averageItemLevel;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }

    public String getThumbnail()
    {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail)
    {
        this.thumbnail = thumbnail;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }
}
