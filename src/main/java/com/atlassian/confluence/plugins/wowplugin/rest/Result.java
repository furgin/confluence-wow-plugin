package com.atlassian.confluence.plugins.wowplugin.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "character")
@XmlAccessorType(XmlAccessType.FIELD)
public class Result
{
    @XmlAttribute
    private String field;

    @XmlAttribute
    private String message;

    public Result()
    {
    }

    public Result(String field, String message)
    {
        this.field = field;
        this.message = message;
    }

    public String getField()
    {
        return field;
    }

    public String getMessage()
    {
        return message;
    }
}
