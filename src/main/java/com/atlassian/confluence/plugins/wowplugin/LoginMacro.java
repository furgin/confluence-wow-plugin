package com.atlassian.confluence.plugins.wowplugin;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;

import java.util.Map;

/**
 */
public class LoginMacro implements Macro {

    private final SettingsManager settingsManager;

    public LoginMacro(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
        String osDestination = "/";
        if (AuthenticatedUserThreadLocal.get() != null) {
            return "<a href=\"" + baseUrl + "/logout.action\">Logout</a>";
        } else {
            return "<a href=\"" + baseUrl + "/login.action\">Login</a>";
        }
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}
