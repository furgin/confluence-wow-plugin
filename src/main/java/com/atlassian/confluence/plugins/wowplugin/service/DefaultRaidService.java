package com.atlassian.confluence.plugins.wowplugin.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.CharacterMapping;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.Raid;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.RaidPlayer;
import com.atlassian.confluence.plugins.wowplugin.rest.RsvpEntity;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.user.EntityException;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;
import net.java.ao.DBParam;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class DefaultRaidService implements RaidService
{
    private final Logger logger = LoggerFactory.getLogger(DefaultRaidService.class);
    private final ActiveObjects activeObjects;
    private final UserManager userManager;

    public DefaultRaidService(ActiveObjects activeObjects, UserManager userManager)
    {
        this.activeObjects = activeObjects;
        this.userManager = userManager;
    }

    @Override
    public void deleteRaid(final String key)
    {
        activeObjects.executeInTransaction((TransactionCallback<Void>) () -> {
            final Raid[] raids = activeObjects.find(Raid.class, "KEY = ?", key);
            for (Raid raid : raids) {
                activeObjects.delete(activeObjects.find(RaidPlayer.class, "RAID_ID = ?", raid.getID()));
                activeObjects.delete(raid);
            }
            return null;
        });

    }

    @Override
    public void createRaid(final String key, final boolean invite)
    {
        Raid raid = activeObjects.executeInTransaction(() -> {
            final Raid r = activeObjects.create(Raid.class,
                    new DBParam("KEY", key),
                    new DBParam("CREATED_DATE", new Date())
            );

            // invite players if asked to
            if (invite)
            {

                final CharacterMapping[] mappings = activeObjects.find(CharacterMapping.class);
                Map<String, List<CharacterMapping>> map = new HashMap<>();
                for (CharacterMapping mapping : mappings) {
                    if (!map.containsKey(mapping.getOwner()))
                        map.put(mapping.getOwner(), new ArrayList<>());
                    map.get(mapping.getOwner()).add(mapping);
                }

                for (Map.Entry<String, List<CharacterMapping>> e : map.entrySet())
                {
                    logger.info("invite [ " + e.getKey() + " ] [ " + e.getValue() + " ]");
                    try
                    {
                        final User user = userManager.getUser(e.getKey());
                        activeObjects.create(RaidPlayer.class,
                                new DBParam("RAID_ID", r.getID()),
                                new DBParam("EMAIL", user.getEmail()),
                                new DBParam("USERNAME", user.getName()),
                                new DBParam("STATUS", RaidPlayer.Status.INVITED));
                    }
                    catch (EntityException e1)
                    {
                        logger.error("could not find user for mapping [ " + e.getKey() + " ]");
                    }
                }

            }

            return r;
        });

        for (RaidPlayer player : raid.getPlayers())
            notifyPlayer(player);
    }

    private void notifyPlayer(RaidPlayer player)
    {
//        final Raid raid = player.getRaid();
//
//        User user = null;
//        try {
//            if (player.getUsername() != null)
//                user = userManager.getUser(player.getUsername());
//        } catch (EntityException e) {
//            // user not found?
//            logger.debug("player [ " + player.getEmail() + " ] does not have confluence user");
//        }
//
//        logger.info("notify player [ " + player.getEmail() + " ] for raid [ " + raid.getKey() + " ]");
//
//        VelocityContext contextMap = new VelocityContext(MacroUtils.defaultVelocityContext());
//        contextMap.put("raid", player.getRaid());
//        contextMap.put("player", player);
//        contextMap.put("user", user);
//        final String body = VelocityUtils
//                .getRenderedTemplate("/com/atlassian/confluence/plugins/wowplugin/raid/invite-to-raid.vm", contextMap);
//
//        Email email = new Email(player.getEmail());
//        email.setFromName("Furgin");
//        email.setSubject("Invited to Raid [ " + raid.getKey() + " ]");
//        email.setBody(body);
//        email.setMimeType("text/html");
//
//        taskManager.addTask("mail", new SingleMailQueueItem(email));
    }

    @Override
    public void deleteFromRaid(final String key, final String email)
    {
        activeObjects.executeInTransaction(() -> {
            logger.info("remove [ " + email + " ] from [ " + key + " ]");
            final Raid raid = activeObjects.find(Raid.class, "KEY = ?", key)[0];
            activeObjects.delete(activeObjects.find(RaidPlayer.class, "EMAIL = ? and RAID_ID = ?", email, raid.getID()));
            return raid;
        });
    }

    @Override
    public void inviteToRaid(final String key, final String email)
    {
        notifyPlayer(activeObjects.executeInTransaction(() -> {
            logger.info("invite [ " + email + " ] to [ " + key + " ]");
            final Raid raid = activeObjects.find(Raid.class, "KEY = ?", key)[0];
            return activeObjects.create(RaidPlayer.class,
                    new DBParam("RAID_ID", raid.getID()),
                    new DBParam("EMAIL", email),
                    new DBParam("STATUS", RaidPlayer.Status.INVITED));
        }));
    }

    @Override
    public void rsvpToRaid(final String key, final String email, final RsvpEntity response)
    {
        activeObjects.executeInTransaction((TransactionCallback<Void>) () -> {
            logger.info("rsvp [ " + email + " ] for [ " + key + " ] set to [ " + response.getStatus() + " ] with [ " + response.getCharacter() + ":" + response.getRole() + " ]");

            final Raid raid = activeObjects.find(Raid.class, "KEY = ?", key)[0];
            final RaidPlayer[] players = activeObjects.find(RaidPlayer.class, "EMAIL = ? and RAID_ID = ?", email, raid.getID());

            for (RaidPlayer player : players)
            {
                player.setStatus(response.getStatus());
                player.setRole(response.getRole());
                player.setCharacter(response.getCharacter());
                player.save();
            }
            return null;
        });
    }

    @Override
    public List<Raid> getRaids()
    {
        return activeObjects.executeInTransaction(() -> Arrays.asList(activeObjects.find(Raid.class,
                Query.select().where("CREATED_DATE IS NOT NULL").order("CREATED_DATE DESC"))));
    }
}
