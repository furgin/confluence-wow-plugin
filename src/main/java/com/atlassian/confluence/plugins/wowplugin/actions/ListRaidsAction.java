package com.atlassian.confluence.plugins.wowplugin.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.Raid;
import com.atlassian.confluence.plugins.wowplugin.service.RaidService;

import java.util.List;

public class ListRaidsAction extends ConfluenceActionSupport {

    private final RaidService raidService;
    private List<Raid> raids;

    public ListRaidsAction(RaidService raidService)
    {
        this.raidService = raidService;
    }

    @Override
    public String execute() throws Exception
    {
        raids = raidService.getRaids();
        return SUCCESS;
    }

    public List<Raid> getRaids()
    {
        return raids;
    }
}
