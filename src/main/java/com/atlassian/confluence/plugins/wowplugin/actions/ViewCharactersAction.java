package com.atlassian.confluence.plugins.wowplugin.actions;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.Character;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.CharacterMapping;
import com.atlassian.confluence.plugins.wowplugin.service.BattleNetService;
import com.atlassian.confluence.user.actions.AbstractUserProfileAction;
import com.atlassian.confluence.user.actions.UserAware;
import com.atlassian.confluence.util.GeneralUtil;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Action to list linked World of Warcraft Characters
 */
public class ViewCharactersAction extends AbstractUserProfileAction implements UserAware
{
    private String username;
    private final ActiveObjects activeObjects;
    private final BattleNetService battleNetService;
    private List<Character> mappings;

    public ViewCharactersAction(ActiveObjects activeObjects, BattleNetService battleNetService)
    {
        this.activeObjects = activeObjects;
        this.battleNetService = battleNetService;
    }

    public String execute() throws Exception
    {
        mappings = new ArrayList<>();
        final List<CharacterMapping> list = activeObjects.executeInTransaction(() -> Arrays.asList(activeObjects.find(CharacterMapping.class, "OWNER = ?", getUsername())));
        for (CharacterMapping mapping : list)
        {
            mappings.add(battleNetService.character(
                    BattleNET.Region.valueOf(mapping.getRegion()),
                    mapping.getRealm(),
                    mapping.getName()));
        }
        return SUCCESS;
    }

    public String getUsername()
    {
        if (StringUtils.isEmpty(username) && getAuthenticatedUser() != null)
            username = getAuthenticatedUser().getName();
        return username;
    }

    public void setUsername(String username)
    {
        if (GeneralUtil.shouldUrlDecode(username))
            username = GeneralUtil.urlDecode(username);
        this.username = username;
    }

    public List<Character> getCharacters()
    {
        return mappings;
    }

}
