package com.atlassian.confluence.plugins.wowplugin;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LongList {

    public static String asString(long[] array) {
        if (array==null) { return ""; }
        return Arrays.stream(array).mapToObj(String::valueOf).collect(Collectors.joining(","));
    }

    public static long[] parse(String list) {
        if(list.isEmpty())
            return new long[0];
        String[] parts = list.split(",");
        List<Long> longList = Arrays.stream(parts).map((s) -> Long.parseLong(s.trim())).collect(Collectors.toList());
        long[] longArray = new long[longList.size()];
        for (int i = 0; i < longList.size(); i++) {
            longArray[i] = longList.get(i);
        }
        return longArray;
    }
}
