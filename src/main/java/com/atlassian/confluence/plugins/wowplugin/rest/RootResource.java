package com.atlassian.confluence.plugins.wowplugin.rest;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.Character;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.plugins.wowplugin.PluginConfiguration;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.CharacterMapping;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.Raid;
import com.atlassian.confluence.plugins.wowplugin.activeobjects.RaidPlayer;
import com.atlassian.confluence.plugins.wowplugin.service.BattleNetService;
import com.atlassian.confluence.plugins.wowplugin.service.CharacterService;
import com.atlassian.confluence.plugins.wowplugin.service.RaidService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.atlassian.sal.api.transaction.TransactionCallback;
import net.java.ao.DBParam;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 */
@Path("/wow")
public class RootResource {
    private final BattleNetService battleNetService;
    private final ActiveObjects activeObjects;
    private final PluginConfiguration pluginConfiguration;
    private final RaidService raidService;
    private final CharacterService characterService;

    @Context
    protected AuthenticationContext authContext;

    public RootResource(BattleNetService battleNetService,
                        ActiveObjects activeObjects,
                        PluginConfiguration pluginConfiguration,
                        RaidService raidService,
                        CharacterService characterService) {
        this.battleNetService = battleNetService;
        this.activeObjects = activeObjects;
        this.pluginConfiguration = pluginConfiguration;
        this.raidService = raidService;
        this.characterService = characterService;
    }

    @GET
    @AnonymousAllowed
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/character")
    public Response getCharacter(@QueryParam("region") String regionName,
                                 @QueryParam("realm") String realm,
                                 @QueryParam("name") String name) {
        if (StringUtils.isEmpty(realm) && StringUtils.isEmpty(name))
            return Response.ok(new CharacterEntityList(Arrays.asList(
                    activeObjects.find(CharacterMapping.class, "OWNER = ?", authContext.getPrincipal().getName())))).build();

        final BattleNET.Region region = BattleNET.Region.valueOf(regionName.toUpperCase());
        Character character = battleNetService.character(region,
                realm, name, Character.Field.ITEMS);
        if (character != null)
            return Response.ok(build(region, character)).build();
        else
            return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @AnonymousAllowed
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/character/{owner}/{region}/{realm}/{name}")
    public Response removeCharacter(@PathParam("owner") final String owner,
                                    @PathParam("region") final String region,
                                    @PathParam("realm") final String realm,
                                    @PathParam("name") final String name) {
        System.out.println(authContext.getPrincipal().getName());
        if (!authContext.getPrincipal().getName().equals(owner))
            return Response.status(Response.Status.FORBIDDEN).build();

        characterService.removeCharacter(owner, region, realm, name);
        return Response.ok().build();
    }

    private CharacterEntity build(BattleNET.Region region, Character character) {
        CharacterEntity characterEntity = new CharacterEntity(region, character, battleNetService);
        return characterEntity;
    }

    @POST
    @AnonymousAllowed
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/character/{owner}")
    public Response addCharacter(@PathParam("owner") final String owner, CharacterEntity character) {
        // TODO: Move to CharacterService

        if (!authContext.getPrincipal().getName().equals(owner))
            return Response.status(Response.Status.FORBIDDEN).build();

        // validate results
        Collection<Result> results = new ArrayList<Result>();

        // validate region
        if (StringUtils.isBlank(character.getRegion()) || !validEnum(BattleNET.Region.class, character.getRegion()))
            results.add(new Result("region", "Invalid Region"));

        // cant proceed without a valid region
        if (!results.isEmpty())
            return Response.status(400).entity(results).build();

        final BattleNET.Region region = BattleNET.Region.valueOf(character.getRegion());

        // validate realm
        if (StringUtils.isBlank(character.getRealm()) || battleNetService.realms(region, character.getRealm()).length == 0)
            results.add(new Result("realm", "Invalid Realm"));

        // validate name
        if (StringUtils.isBlank(character.getName()))
            results.add(new Result("name", "Invalid Name"));

        // cant proceed without a valid name realm and region
        if (!results.isEmpty())
            return Response.status(400).entity(results).build();

        // load the character from blizzard to see if its valid
        final Character character1 = battleNetService.character(region,
                character.getRealm(),
                character.getName(),
                Character.Field.ITEMS);

        if (character1 == null) {
            results.add(new Result("name", "Character not found"));
        } else {
            // valid data -- check for conflicts
            CharacterMapping[] mappings = activeObjects.find(
                    CharacterMapping.class, "OWNER = ? AND REALM = ? AND NAME = ? AND REGION = ?", new String[]{
                    owner, character1.getRealm(), character1.getName(), region.toString()
            });
            if (mappings.length > 0)
                results.add(new Result("name", "Already exists"));
        }

        if (!results.isEmpty())
            return Response.status(400).entity(results).build();

        if (character1 != null) {
            activeObjects.executeInTransaction(new TransactionCallback<CharacterMapping>() {
                public CharacterMapping doInTransaction() {
                    return activeObjects.create(
                            CharacterMapping.class,
                            new DBParam("NAME", character1.getName()),
                            new DBParam("REALM", character1.getRealm()),
                            new DBParam("REGION", region.toString()),
                            new DBParam("OWNER", owner)
                    );
                }
            });
        }
        return Response.ok(build(region, character1)).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/raid/{key}")
    @AnonymousAllowed
    public Response deleteRaid(@PathParam("key") final String key) {
        if (!pluginConfiguration.isPermittedToCreateRaids())
            return Response.status(Response.Status.UNAUTHORIZED).build();
        raidService.deleteRaid(key);
        return Response.ok().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/raid")
    @AnonymousAllowed
    public Response createRaid(final RaidEntity raid) {
        if (!pluginConfiguration.isPermittedToCreateRaids())
            return Response.status(Response.Status.UNAUTHORIZED).build();
        Collection<Result> results = new ArrayList<Result>();
        if (!isValid(raid.getKey()))
            results.add(new Result("key", "Invalid Key, ABC-123"));
        if (!results.isEmpty())
            return Response.status(400).entity(results).build();
        raidService.createRaid(raid.getKey(), raid.isInvite());
        return Response.ok().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/raid/{key}/{email}")
    @AnonymousAllowed
    public Response inviteToRaid(@PathParam("key") final String key, @PathParam("email") final String email) {
        if (!pluginConfiguration.isPermittedToCreateRaids())
            return Response.status(Response.Status.UNAUTHORIZED).build();

        Collection<Result> results = new ArrayList<Result>();
        if (!isValid(key))
            results.add(new Result("key", "Invalid Key, ABC-123"));
        if (!results.isEmpty())
            return Response.status(400).entity(results).build();

        raidService.inviteToRaid(key, email);

        return Response.ok().build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/raid/{key}/{email}")
    @AnonymousAllowed
    public Response rsvpToRaid(@PathParam("key") final String key,
                               @PathParam("email") final String email,
                               final RsvpEntity response) {

        if (!pluginConfiguration.isPermittedToCreateRaids())
            return Response.status(Response.Status.UNAUTHORIZED).build();

        Collection<Result> results = new ArrayList<Result>();
        if (!isValid(key))
            results.add(new Result("key", "Invalid Key, ABC-123"));
        if (!results.isEmpty())
            return Response.status(400).entity(results).build();

        raidService.rsvpToRaid(key, email, response);

        return Response.ok().build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/raid/{key}/{email}")
    @AnonymousAllowed
    public Response deleteFromRaid(@PathParam("key") final String key, @PathParam("email") final String email) {
        if (!pluginConfiguration.isPermittedToCreateRaids())
            return Response.status(Response.Status.UNAUTHORIZED).build();

        Collection<Result> results = new ArrayList<Result>();

        if (!isValid(key))
            results.add(new Result("key", "Invalid Key, ABC-123"));

        if (!results.isEmpty())
            return Response.status(400).entity(results).build();

        raidService.deleteFromRaid(key, email);

        return Response.ok().build();
    }

    private boolean isValid(String key) {
        if (StringUtils.isBlank(key))
            return false;

        String validChars = "abcdefghijklmnopqrstuvwxyz-0123456789";
        for (int i = 0; i < key.length(); i++) {
            String s = key.toLowerCase().substring(i, i + 1);
            if (!validChars.contains(s))
                return false;
        }
        return true;
    }

    private boolean validEnum(Class<? extends Enum> e, String val) {
        final Enum[] enums = e.getEnumConstants();
        for (Enum ane : enums)
            if (ane.toString().equals(val))
                return true;
        return false;
    }


}
