package com.atlassian.confluence.plugins.wowplugin;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.model.Character;
import au.com.angry.battlenet.model.CharacterClass;
import au.com.angry.battlenet.model.CharacterItem;
import au.com.angry.battlenet.model.CharacterRace;
import au.com.angry.battlenet.model.Guild;
import au.com.angry.battlenet.model.GuildMember;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.wowplugin.service.BattleNetService;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 */
public class GuildGearMacro implements Macro {
    private final BattleNetService battleNetService;
    private final MacroHelper macroHelper;
    private final PluginConfiguration pluginConfiguration;

    private static class Slot {

        public static Slot of(String name, boolean display) {
            return of(name, display, null);
        }

        public static Slot of(String name, boolean display, Function<Character, CharacterItem> get) {
            return new Slot(name, display, get);
        }

        public final String name;
        public final boolean display;
        public final Function<Character, CharacterItem> get;

        private Slot(String name, boolean display, Function<Character, CharacterItem> get) {
            this.name = name;
            this.display = display;
            this.get = get;
        }
    }

    private final Slot[] slots = new Slot[]{
            Slot.of("ammo", false),
            Slot.of("Head", true, (ch) -> ch.getItems().getHead()),
            Slot.of("Neck", true, (ch) -> ch.getItems().getNeck()),
            Slot.of("Shoulder", true, (ch) -> ch.getItems().getShoulder()),
            Slot.of("shirt", false),
            Slot.of("Chest", true, (ch) -> ch.getItems().getChest()),
            Slot.of("Waist", true, (ch) -> ch.getItems().getWaist()),
            Slot.of("Legs", true, (ch) -> ch.getItems().getLegs()),
            Slot.of("Feet", true, (ch) -> ch.getItems().getFeet()),
            Slot.of("Wrist", true, (ch) -> ch.getItems().getWrist()),
            Slot.of("Hands", true, (ch) -> ch.getItems().getHands()),
            Slot.of("Finger 1", true, (ch) -> ch.getItems().getFinger1()),
            Slot.of("Finger 2", true, (ch) -> ch.getItems().getFinger2()),
            Slot.of("Trinket 1", true, (ch) -> ch.getItems().getTrinket1()),
            Slot.of("Trinket 2", true, (ch) -> ch.getItems().getTrinket2()),
            Slot.of("Back", true, (ch) -> ch.getItems().getBack()),
            Slot.of("Main Hand", true, (ch) -> ch.getItems().getMainHand()),
            Slot.of("Off Hand", true, (ch) -> ch.getItems().getOffHand()),
            Slot.of("ranged", false),
            Slot.of("tabard", false),
            Slot.of("bag 1", false),
            Slot.of("bag 2", false),
            Slot.of("bag 3", false),
            Slot.of("bag 4", false)
    };

    public GuildGearMacro(BattleNetService battleNetService,
                          XhtmlContent xhtmlContent, PluginConfiguration pluginConfiguration) {
        this.battleNetService = battleNetService;
        this.pluginConfiguration = pluginConfiguration;
        this.macroHelper = new MacroHelper(xhtmlContent);
    }

    public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException {
        String guildName = params.get("guild");
        String realmName = params.get("realm");
        String regionName = params.get("region");

        if (StringUtils.isEmpty(realmName))
            realmName = pluginConfiguration.getDefaultRealm();

        if (StringUtils.isEmpty(regionName))
            regionName = pluginConfiguration.getDefaultRegion();

        final BattleNET.Region region = BattleNET.Region.valueOf(regionName.toUpperCase());

        final Guild guild = battleNetService.guild(region, realmName, guildName, Guild.Field.MEMBERS);

        if (guild != null) {

            StringBuilder out = new StringBuilder("<table>" +
                    "<tr>" +
                    "<th>Name</th>" +
                    "<th>iLvl</th>");
            List<Slot> filteredSlots = Arrays.stream(slots).filter(s -> s.display).collect(Collectors.toList());
            for (Slot slot : filteredSlots) {
                out.append("<th>").append(slot.name).append("</th>");
            }
            out.append("<th>Avg</td></th>");

            Collection<GuildMember> members = Arrays.stream(guild.getMembers())
                    .filter(m -> m != null && m.getCharacter() != null && m.getCharacter().getLevel() == 110)
                    .collect(Collectors.toList());

            List<Character> characters = members.stream().filter(Objects::nonNull)
                    .map(member -> {
                final Character ch = member.getCharacter();
                return battleNetService.character(region,
                        ch.getRealm(), ch.getName(),
                        Character.Field.ITEMS,
                        Character.Field.TALENTS);
            }).collect(Collectors.toList());

            characters.sort((a, b) -> {
                if (b == null && a != null)
                    return 1;
                if (b != null && a == null)
                    return -1;
                if (b == null && a == null)
                    return 0;
                if (b.getItems() == null && a.getItems() != null)
                    return 1;
                if (b.getItems() != null && a.getItems() == null)
                    return -1;
                if (b.getItems() == null && a.getItems() == null)
                    return 0;
                return b.getItems().getAverageItemLevelEquipped() - a.getItems().getAverageItemLevelEquipped();
            });

            for (Character ch : characters) {
                final CharacterClass characterClass = battleNetService.characterClass(region, ch.getCharacterClass());
                final CharacterRace characterRace = battleNetService.characterRace(region, ch.getRace());

                out.append("<tr><td>");
                out.append(macroHelper.characterMacro(region, ch, conversionContext));
                out.append("</td>");
                out.append(
                        String.format(
                                "<td>%s</td>",
                                ch.getItems() == null ? "?" :
                                        (String.valueOf(ch.getItems().getAverageItemLevelEquipped()) +
                                                "/" +
                                                String.valueOf(ch.getItems().getAverageItemLevel()))
                        )
                );
                float total = 0;
                int count = 0;
                for (Slot slot : filteredSlots) {
                    count++;
                    if (slot == null || slot.get == null) {
                        out.append("<td>0</td>");
                    } else {
                        CharacterItem characterItem = slot.get.apply(ch);
                        if (characterItem == null) {
                            out.append("<td>0</td>");
                        } else {
                            out.append(String.format("<td>%d</td>", characterItem.getItemLevel()));
                            total += characterItem.getItemLevel();
                        }
                    }
                }

                out.append(String.format("<td>%.1f</td>", (float) (total / (float) count)));
                out.append("</tr>");
            }

            out.append("</table>");
            return out.toString();
        } else {

            return "Could not load guild: " + guildName;
        }

    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
