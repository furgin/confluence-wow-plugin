package com.atlassian.confluence.plugins.wowplugin.rest;

import javax.xml.bind.annotation.*;

/**
 */
@XmlRootElement(name="item")
@XmlAccessorType(XmlAccessType.FIELD)
public class CharacterItemEntity
{
    @XmlAttribute
    private long itemId;

    @XmlAttribute
    private String icon;

    @XmlAttribute
    private int slot;

    @XmlElement
    private String rel;

    public CharacterItemEntity()
    {
    }

    public CharacterItemEntity(long itemId, String icon, int slot, String rel)
    {
        this.itemId = itemId;
        this.icon = icon;
        this.slot = slot;
        this.rel = rel;
    }

    public CharacterItemEntity(String icon, int slot)
    {
        this(0,icon,slot,"");
    }

    public long getItemId()
    {
        return itemId;
    }

    public void setItemId(int itemId)
    {
        this.itemId = itemId;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public int getSlot()
    {
        return slot;
    }

    public void setSlot(int slot)
    {
        this.slot = slot;
    }

    public String getRel()
    {
        return rel;
    }

    public void setRel(String rel)
    {
        this.rel = rel;
    }
}
