package com.atlassian.confluence.plugins.wowplugin.service;

import au.com.angry.battlenet.model.Guild;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.cache.ConfluenceManagedCache;
import com.atlassian.util.concurrent.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CachingService {

    private static class CacheHolder<T> {
        private final T data;
        private final long timestamp;

        private CacheHolder(T data, long timestamp) {
            this.data = data;
            this.timestamp = timestamp;
        }
    }

    private static class CacheInfo {
        private final long expiry;

        private CacheInfo(long expiry) {
            this.expiry = expiry;
        }
    }

    private final Map<Class, CacheInfo> cacheInfoMap = new HashMap<>();

    private final Logger logger = LoggerFactory.getLogger(DefaultBattleNetService.class);
    private final String cacheName;
    private final CacheManager cacheManager;

    public CachingService(String cacheName, CacheManager cacheManager) {
        this.cacheName = cacheName;
        this.cacheManager = cacheManager;

        cacheInfoMap.put(DefaultBattleNetService.CachedCharacter.class, new CacheInfo(TimeUnit.MINUTES.toMillis(30)));
        cacheInfoMap.put(Guild.class, new CacheInfo(TimeUnit.MINUTES.toMillis(30)));
    }

    public <T> void put(Class<T> clazz, Object key, T t) {
        getCache(clazz).put(key, new CacheHolder<>(t, System.currentTimeMillis()));
    }

    public <T> T get(Class<T> clazz, Object key, Supplier<T> supplier) {
        final Cache<Object, CacheHolder<T>> cache = getCache(clazz);
        CacheInfo cacheInfo = cacheInfoMap.get(clazz);
        boolean refreshCache = false;
        long time = System.currentTimeMillis();
        CacheHolder<T> cachedHolder = cache.get(key);
        if (!cache.getKeys().contains(key) || cachedHolder == null) {
            logger.debug("object not cached [ " + clazz.getName() + " ] for key [ " + key + " ]");
            // not cached, refresh
            refreshCache = true;
        }
        if (cachedHolder != null && cacheInfo != null) {
            long age = time - cachedHolder.timestamp;
            if (age > cacheInfo.expiry) {
                logger.debug("object cache expired [ " + clazz.getName() + " ] for key [ " + key + " ] age [ " + age + " ] max [ " + cacheInfo.expiry + " ]");
                refreshCache = true;
            }
        }
        if (refreshCache) {
            logger.debug("refresh cache [ " + clazz.getName() + " ] for key [ " + key + " ]");
            final T value = supplier.get();
            if (value != null)
                put(clazz, key, value);
        } else {
            logger.debug("using cache [ " + clazz.getName() + " ] for key [ " + key + " ]");
        }
        //noinspection ConstantConditions
        return cache.get(key).data;
    }

    private <T> Cache<Object, T> getCache(Class clazz) {
        return cacheManager.getCache(cacheName + ":" + clazz.getSimpleName());
    }

    public void flush() {
        final Collection caches = cacheManager.getManagedCaches();
        for (Object obj : caches) {
            final ConfluenceManagedCache cache = (ConfluenceManagedCache) obj;
            if (cache.getName().startsWith(cacheName)) {
                logger.debug("flush cache [ {} ]", cache.getName());
                cacheManager.getCache(cache.getName()).removeAll();
            }
        }
    }

}
