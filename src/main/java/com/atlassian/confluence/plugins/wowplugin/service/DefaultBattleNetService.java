package com.atlassian.confluence.plugins.wowplugin.service;

import au.com.angry.battlenet.BattleNET;
import au.com.angry.battlenet.Named;
import au.com.angry.battlenet.impl.DefaultBattleNET;
import au.com.angry.battlenet.model.*;
import au.com.angry.battlenet.model.Character;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.util.http.HttpRetrievalService;
import com.atlassian.util.concurrent.Supplier;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultBattleNetService implements BattleNetService, DisposableBean, InitializingBean {
    public static class CachedCharacter {
        private final Character.Field[] fields;
        private final Character character;

        private CachedCharacter(Character.Field[] fields, Character character) {
            this.character = character;
            this.fields = fields;
        }

        public int hashCode() {
            return new HashCodeBuilder()
                    .append(this.character)
                    .append(this.fields)
                    .toHashCode();
        }

        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            final CachedCharacter other = (CachedCharacter) obj;
            return new EqualsBuilder()
                    .append(this.character, other.character)
                    .append(this.fields, other.fields)
                    .isEquals();
        }
    }

    private final Logger logger = LoggerFactory.getLogger(DefaultBattleNetService.class);
    private final BattleNET battleNET;
    private final CachingService cachingService;

    public DefaultBattleNetService(HttpRetrievalService httpRetrievalService, CacheManager cacheManager) {
        battleNET = new DefaultBattleNET(new ConfluenceRemoteConnection(httpRetrievalService),
                "buprd3jsv6azaf4rmqxnbptrx6fc8at6");
        cachingService = new CachingService(DefaultBattleNetService.class.getName(), cacheManager);
    }

    public CharacterRace characterRace(final Region region, int raceId) {
        for (CharacterRace race : races(region))
            if (race.getId() == raceId) return race;
        return null;
    }

    public CharacterClass characterClass(final Region region, int classId) {
        for (CharacterClass characterClass : classes(region))
            if (characterClass.getId() == classId) return characterClass;
        return null;
    }

    public Character character(final Region region, final String realm, final String name, final Character.Field... fields) {
        final Supplier<CachedCharacter> supplier = () -> {
            logger.debug("load character [ {} ] [ {} ] with [ {} ] from [ {} ]", realm, name, j(fields), region);
            return new CachedCharacter(fields, battleNET.character(region, realm, name, fields));
        };

        final String key = region + "-" + realm + "-" + name;
        CachedCharacter ch = cachingService.get(CachedCharacter.class, key, supplier);

        // requested fields aren't found in the cached object, refresh it
        if (ch == null || !Arrays.asList(ch.fields).containsAll(Arrays.asList(fields))) {
            Character.Field[] allFields = (ch == null ? fields : f(fields, ch.fields));
            logger.debug("refresh character [ {} ] [ {} ] to include [ {} ] from [ {} ]", realm, name, j(allFields), region);
            ch = new CachedCharacter(allFields, battleNET.character(region, realm, name, allFields));
            cachingService.put(CachedCharacter.class, key, ch);
        }

        return ch.character;
    }

    private String j(Character.Field[] fields) {
        StringBuilder buf = new StringBuilder();
        for (Character.Field field : fields) {
            if (buf.toString().length() > 0)
                buf.append(",");
            buf.append(field.getName());
        }
        return buf.toString();
    }

    private Character.Field[] f(Character.Field[]... fieldArray) {
        List<Character.Field> fields = new ArrayList<>();
        for (Character.Field[] aF : fieldArray)
            for (Character.Field anAF : aF)
                if (!fields.contains(anAF))
                    fields.add(anAF);
        return fields.toArray(new Character.Field[fields.size()]);
    }

    @Override
    public Item item(final Region region, final long itemId, final long... bonusList) {
        String bonusListString = Arrays.stream(bonusList).mapToObj(String::valueOf).collect(Collectors.joining(","));
        return cachingService.get(Item.class, region + "-item-" + itemId + "-bl-" + bonusListString, () -> {
            logger.debug("load item [ {} ]", itemId);
            return battleNET.item(region, itemId, bonusList);
        });
    }

    public Guild guild(final Region region, final String realm, final String name, final Guild.Field... fields) {
        return cachingService.get(Guild.class, region + "-" + realm + "-" + name + "-" + k(fields), () -> {
            logger.debug("load guild [ {} ] [ {} ] [ {} ] from [ {} ]", realm, name, k(fields), region);
            return battleNET.guild(region, realm, name, fields);
        });
    }

    public Spell spell(final Region region, final long spellId) {
        return cachingService.get(Spell.class, region + "-spell-" + spellId, () -> {
            logger.debug("load spell [ {} ]", spellId);
            return battleNET.spell(region, spellId);
        });
    }

    public Realm[] realms(final Region region, final String... realms) {
        return cachingService.get(Realm[].class, region + "-realms-" + k(realms), () -> {
            logger.debug("load realms [ {} ]", k(realms));
            return battleNET.realms(region, realms);
        });
    }

    public CharacterRace[] races(final Region region) {
        return cachingService.get(CharacterRace[].class, region + "-races", () -> {
            logger.debug("load races");
            return battleNET.races(region);
        });
    }

    public CharacterClass[] classes(final Region region) {
        return cachingService.get(CharacterClass[].class, region + "-classes", () -> {
            logger.debug("load classes");
            return battleNET.classes(region);
        });
    }

    public Battlegroup[] battlegroups(final Region region) {
        return cachingService.get(Battlegroup[].class, region + "-battlegroups", () -> {
            logger.debug("load battlegroups");
            return battleNET.battlegroups(region);
        });
    }

    public AchievementCategory[] achievements(final Region region) {
        return cachingService.get(AchievementCategory[].class, region + "-achievements", () -> {
            logger.debug("load achievements");
            return battleNET.achievements(region);
        });
    }

    private String k(Named[] array) {
        StringBuilder b = new StringBuilder();
        boolean first = true;
        for (Named named : array) {
            if (!first)
                b.append("-");
            b.append(named.getName());
            first = false;
        }
        return b.toString();
    }

    private String k(String[] array) {
        StringBuilder b = new StringBuilder();
        boolean first = true;
        for (String named : array) {
            if (!first)
                b.append("-");
            b.append(named);
            first = false;
        }
        return b.toString();
    }

    public void destroy() throws Exception {
        cachingService.flush();
    }

    public void afterPropertiesSet() throws Exception {
        cachingService.flush();
    }
}
