AJS.$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

WOW = function($) {

    return {

        ajax: function (type, url, data, success, error)
        {
            var d = data;
            if (d instanceof Object) {
                d = JSON.stringify(d);
            }

            $.ajax({
                url: url,
                data: d,
                type: type,
                dataType: "text json",
                contentType: "application/json",
                success: success,
                error: error
            })
        },

        notify: function(message,classes) {

            if($("#notify").length == 0)
                $("body").append("<div id='notify'></div>");

            $("#notify").append("<div class='aui-message "+classes+" shadowed'>"+message+"</div>")
                .fadeIn(500)
                .delay(1000)
                .fadeOut(500, function(){
                    $(this).remove();
                });
        },

        makePopup: function(formSelector, showSelector, popupID, args){
            var opts = {
                title: 'Add',
                submit: 'Add',
                width: 600,
                height: 400,
                hasPanels: false,
                method: "POST",
                open: function(button){},
                verify: function(form){ return true; },
                addPanels: function(dialog){
                    dialog.addPanel("Form", formSelector, "form-body");
                },
                success: function(data){
                    window.location.reload();
                },
                failure: function (form, data)
                {
                    $(form).find(".error").remove();
                    for (var i = 0; i < data.length; i++)
                    {
                        var res = data[i];
                        $("[name='" + res.field + "']",form).after("<div class=\"error\">"+res.message+"</div>")
                    }
                },
                data: function(form){ return form.serializeObject() },
                url: function(form) { return AJS.Confluence.getContextPath() + form.attr("action") }
            };
            opts = $.extend(opts,args);

            var popup = new AJS.Dialog({
                width:opts.width,
                height:opts.height,
                id: popupID,
                closeOnOutsideClick: true
            });

            var submitForm = function (dialog){
                // reference to panel
                var selector = opts.hasPanels ? formSelector[dialog.getCurrentPanel().id] : formSelector;

                dialog.gotoPage(2);

                var form = $(selector);
                if(!opts.verify(form)){
                    dialog.gotoPage(1);
                    return;
                }

                var data = opts.data(form);
                WOW.ajax(opts.method, opts.url(form), data, function () {
                    dialog.hide();
                    dialog.gotoPage(1);
                    opts.success(data);
                    return false;
                }, function (jqXHR) {
                    dialog.gotoPage(1);
                    opts.failure(form,jQuery.parseJSON(jqXHR.responseText));
                    return false;
                });
            };

            if($("#loading").length == 0)
                $("body").append("<div id='loading'><h1>Loading...</h1></div>");

            popup.addPage("form-panel");
            popup.addPage("loading-panel");

            popup.gotoPage(1);

            opts.addPanels(popup);

            popup.addHeader(opts.title);
            popup.addCancel("Cancel", function(){popup.hide();}, "cancel-button");
            popup.addButton(opts.submit, submitForm, "add-button");

            popup.gotoPage(2);
            popup.addPanel("Loading", "#loading", "loading-body", 1);

            popup.gotoPage(1);

            var form = $(formSelector);
            form.find("input")
                .keypress(function(e) {
                    if(e.which == 10 || e.which == 13) {
                        e.preventDefault();
                        form.submit();
                    }
                });

            form.submit(function(e) {
                e.preventDefault();
                submitForm(popup)
            });

            $(showSelector).click(function(e) {
                e.preventDefault();
                opts.open($(this));
                popup.show();
                return false;
            });
        },

        makeConfirm: function(message, showSelector, f){
            $(showSelector).click(function(e){
                e.preventDefault();
                if(confirm(message)==true) {
                    f(this);
                }
            });
        }

    }

}(AJS.$);

WOW.Raid = function ($)
{
    return {
        add: function wow_raid_add()
        {
            var createRaidDialog = new AJS.Dialog({
                width: 500, height: 220, id: "create-raid-dialog",
                closeOnOutsideClick: true
            });

            var template = $(WOW.Templates.WowPlugin.createRaidDialog());

            createRaidDialog.addHeader("Create Raid", "create-raid-title");
            createRaidDialog.addPanel("Create Raid", template);

            var createRaid = function() {
                var createRaidForm = $("#create-raid-form");
                var raid = {
                    key: createRaidForm.find("input[name='key']").val()
                };
                WOW.ajax("POST",AJS.Confluence.getContextPath() + "/rest/wow/1.0/wow/raid", raid, function () {
                    alert(raid.key+" created.");
                    window.location.reload();
                }, function (jqXHR) {
                    if(jqXHR.status==400) {
                        alert("Raid not created: Invalid key");
                    } else {
                        alert("Raid not created: "+jqXHR.statusText);
                    }
                });
            };

            createRaidDialog.addButton("Create", function ()
            {
                createRaid();
                return false;
            }, "#");
            createRaidDialog.addCancel(AJS.I18n.getText("cancel.name"), function ()
            {
                createRaidDialog.remove();
            });
            createRaidDialog.show();

            var $dialog = createRaidDialog.popup.element;
            $("form", $dialog).submit(function(e) {
                e.preventDefault();
                createRaid();
            });

//            $("form#create-raid-form").keyup(function(e){alert("fuck you");return e.which!==13});
        }
    }

}(AJS.$);

WOW.Characters = function ($)
{
    return {

        loadSummary: function wow_char_load_summary(index, el)
        {
            var id = "character-tooltip-" + index;
            var selector = "#" + id;
            $("body").append("<div id='" + id + "' class='character-tooltip'>Loading...</div>");

            var name = el.attr("data-name"),
                region = el.attr("data-region"),
                realm = el.attr("data-realm");

            $(".character-name", el).mousemove(
                function (e)
                {
                    $(selector).addClass("visible").css({
                        "display": "absolute",
                        "left": e.pageX + 2,
                        "top": e.pageY + 2});
                }).mouseleave(function ()
                {
                    $(selector).removeClass("visible");
                });

            function stringStartsWith (string, prefix) {
                return string.slice(0, prefix.length) == prefix;
            }

            $.ajax({
                url: AJS.Confluence.getContextPath() + "/rest/wow/1.0/wow/character.json",
                data: "name=" + name + "&realm=" + realm + "&region=" + region,
                type: "GET",
                accepts: "application/json",
                success: function (msg)
                {
                    var characterClass = "character-" + msg["class"].toLowerCase().replace(" ", "-");
                    var html = AJS.template.load("character-template-tooltip").fill({
                        name: msg.name,
                        realm: msg.realm,
                        region: msg.region,
                        thumbnail: msg.thumbnail,
                        "character-css-class": characterClass,
                        "class": msg["class"],
                        "spec": msg["spec"],
                        race: msg.race,
                        "averageItemLevel": msg.averageItemLevel,
                        level: msg.level
                    }).toString();
                    html = html.split("\n").filter(function(line){
                        return !stringStartsWith(line,"//");
                    }).join("\n");
                    console.log(typeof html);
                    console.log(html);
                    $(selector).html(html);

                    $(".character-name", el)
                        .addClass(characterClass)
                        .css("cursor", "pointer")
                        .click(function ()
                        {
                            // TODO region
                            window.open("http://us.battle.net/wow/en/character/" + msg.realm + "/" + msg.name + "/simple");
                            return false;
                        });
                },
                error: function ()
                {
                    $(selector).html("<span class='character-info'>Error loading character.</span>");
                }
            });
        }

    }
}(AJS.$);

AJS.toInit(function ($)
{
    $("button.auto-button").click(function() {

        if (!($(this).hasClass("confirm") && !confirm($(this).attr("confirm")))) {
            $.ajax({
                url: AJS.Confluence.getContextPath() + $(this).attr('ajax-url'),
                type: $(this).attr('ajax-type'),
                success: function () {
                    window.location.reload();
                },
                error: function (jqXHR) {
                    alert('Error: ' + jqXHR.statusText);
                }
            })
        }
    });

    $(".character-summary").each(function (index)
    {
        WOW.Characters.loadSummary(index, $(this));
    });

});
