Development Loop
================

Requirements
------------

* Java 8
* Maven (mvnvm.org)

Development
-----------

This plugin using atlassian quickreload to create a fast and simple dev loop.

Terminal 1:

* mvn amps:debug -Dproduct.version=6.0.3 -DskipTests

Terminal 2 (when you make changes):

* mvn package

